import React, {Component} from 'react';
import '../styles/App.css';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
// import './App.less'
import './App.css'
class App extends Component {
  render() {
      const Home = () => (
          <div>
              <p>This is a beautiful Home Page</p>
              <p>And the url is '/'</p>
          </div>
      );

      const MyProfile = () => (
          <div>
              <p>Username:XXX</p>
              <p>Gender:Female</p>
              <p>Work:IT Industry</p>
              <p>Website:'/my-profile'</p>
          </div>
      );

      const About = () => (
          <div>
              <p>Company:ThoughtWorks Local</p>
              <p>Location:Xi`an</p>
              <p>For more information,please</p>
              <p>view our<Link to="/">website</Link></p>
          </div>
      );


    return (
      <div className="app">
          <Router>
              <ul>
                  <li>
                      <Link to="/about">About Us</Link>
                  </li>
                  <li>
                      <Link to="/myProfile">My Profile</Link>
                  </li>
                  <li>
                      <Link to="/">Home</Link>
                  </li>
              </ul>

              <Route exact path="/" component={Home} />
              <Route path="/myProfile" component={MyProfile} />
              <Route path="/about" component={About} />
          </Router>
      </div>
    );
  }
}

export default App;
